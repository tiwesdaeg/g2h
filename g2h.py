#!/usr/bin/env python3

# Function to convert gopher links to html links

def link_conv(string):
    string = string.strip('\n')
    string = string.strip(string[0])
    strsplit = string.split('\t')
    if len(strsplit) > 2:
        if strsplit[2] != 'localhost':
            link = '<a href="http://' + strsplit[2] + strsplit[1] + '">' + strsplit[0] + '</a>\n'
        else:
            link = '<a href="' + strsplit[1] + '">' + strsplit[0] + '</a>\n'
    else:
        link = '<a href="' + strsplit[1] + '">' + strsplit[0] + '</a>\n'
    return link

new_file = 'index.html'
title = 'Test Site'

entry = open(new_file,'w')
entry.write('<!DOCTYPE html>\n')
entry.write('<html>\n')
entry.write('<head>\n')
entry.write('<title>\n')
entry.write(title + '\n')
entry.write('</title>\n')
entry.write('<body style="white-space: pre; font-family: \'Courier New\', monospace;">')
with open('gophermap') as f:
    for line in f:
        if '\t' in line:
            if line[0] == 'i':
                line = line.strip(line[0])
                line = line.split('\t')
                entry.write(line[0] + '\n')
            else:
                entry.write(link_conv(line))
        else:
            entry.write(line)
entry.write('</body>\n')
entry.write('</html>\n')

